:stylesheet: /Users/keesepema/skins/adoc-riak.css

== Handleiding bouwen auto's


image::car.jpg[title=Het eindresultaat, 512x512]

=== Verzamelen
Verzamel de benodigde materialen: Arduino bord, motor driver (motorshield), DC motoren, wielen, chassis, batterij, en andere onderdelen.
Ze zitten allemaal in het zakje wat je bij de twee chassisdelen hebt gekregen.

=== Folie
Het is niet strikt noodzakelijk maar het verwijderen van de bruine folie op de chassis platen is zeer aan te raden, omdat je tijdens het bouwen veel meer zicht hebt op de onderdelen die je later moet koppelen.
Je bent hier naar verwachting wel een klein uurtje mee bezig.
Het beste werkt een klein stukje loskrijgen van de folie en daarna met je duim over de folie 'rubben'. Zo heb je kasn dat er ineens een heel stuk loslaat.


image::folie.jpg[tile=Folie verwijderen,350x350]


=== Motoren

Bevestig de motoren op het chassis. 

image::motoren.jpg[title= Zo ziet een motor eruit,350x350]

Je hebt deze ophangbeugeltjes en schroeven nodig.

image::motor_schroef.jpg[title=Ophangbeugels en schroeven voor de bevestiging van je motor,350x350]

=== Arduino 
Pak nu de andere chassisplaat. Deze komt straks bovenaan.

image::arduino.jpg[title=Arduino, 315x350]

Bevestig de Arduino op de bovenste chassisplaat. Dus niet op de plaat waar je motoren op zitten.

De punten die in de groene cirkel staan, zijn de bevestigingspunten voor de Arduino.
Het is jammer dat op de chassisplaat geen aparte gaatjes zijn geboord voor debevestiging van de Arduino.

image::arduiono_aansluiting.jpg[title= Bevestigingsgaatjes arduino,350,315]




